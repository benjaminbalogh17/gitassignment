package ro.ase.cts.g1074.tema2;

public class Medic implements OperatiiUser{
	
	private String nume;
	private float comision;
	
	public Medic(String nume, float comision) {
		super();
		this.nume = nume;
		this.comision = comision;
	}

	@Override
	public void modificaComision(float comisionNou) {
		this.comision = comisionNou;
	}

}
